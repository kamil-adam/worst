
; Worst->Lua

; Base codegen
import lua/text

; Redefine functions in Lua to speed things up
import lua/jit/redef

; Make the whole text gen thing in Lua
; or at least anything that uses words that will be used in regular code

; Words like add, and, if, etc., to make writing functions identical

; Compile definitions completely without needing any special words

; export-all

; vi: ft=scheme

